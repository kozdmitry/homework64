import axios from "axios";

const AxiosPosts = axios.create({
    baseURL: "https://js9-kozyrev-blogforlaba-default-rtdb.firebaseio.com",
});

export default AxiosPosts;