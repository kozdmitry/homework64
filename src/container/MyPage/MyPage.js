import React from 'react';
import {
    AppBar,
    Box,
    Button,
    Grid,
    GridList,
    GridListTile,
    IconButton,
    Link,
    Toolbar,
    Typography
} from "@material-ui/core";
import {classes} from "istanbul-lib-coverage";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import Home from "../../components/Pages/Home/Home";
import Contacts from "../../components/Pages/Contacts/Contacts";
import Gallery from "../../components/Pages/Gallery/Gallery";
import Add from "../../components/Pages/Add/Add";

const MyPage = () => {
    return (
        <div Container maxWidth="sm" className="Container">
            <AppBar position="static">
                <Toolbar style={{paddingRight: 50}}>
                    <Grid  container
                           direction="row"
                           justify="space-between"
                           alignItems="center">
                        <Typography variant="h6" className={classes.title}>
                            My page
                        </Typography>
                        <Typography className={classes.root}>
                            <Link href="/" color="inherit">Home</Link>
                        </Typography>
                        <Typography className={classes.root}>
                            <Link href="/add" color="inherit">Add</Link>
                        </Typography>
                        <Typography className={classes.root}>
                            <Link href="/contacts" color="inherit">Contacts</Link>
                        </Typography>
                        <Typography className={classes.root}>
                            <Link href="/gallery" color="inherit">Gallery</Link>
                        </Typography>
                    </Grid>

                </Toolbar>
        </AppBar>
            <Router>
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route exact path="/add" component={Add}/>
                    <Route exact path="/contacts" component={Contacts}/>
                    <Route exact path="/gallery" component={Gallery}/>
                    <Route exact path="/home/:name" component={Home}/>
                </Switch>
            </Router>
        </div>
    );
};

export default MyPage;