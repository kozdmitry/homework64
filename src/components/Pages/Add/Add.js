import React, {useState} from 'react';
import "./Add.css";
import axiosPosts from "../../../axiosPosts";

const Add = props => {
    const id = props.match.params.id;

    const [post, setPost] = useState({
        name: '',
        discription: '',
    });

    const postDataChanged = event => {
        const name = event.target.name;
        const value = event.target.value;

        setPost(prevState => ({
            ...prevState,
            [name]: value
        }));
    };
    const postHandler = async (e) => {
        e.preventDefault();

        const posts = {
            date: new Date(),
            name: post.name,
            discription: post.discription
        }

        try {
            await axiosPosts.post('/posts.json', posts);
        } finally {
            props.history.push('/');
        }
    };



    return (
        <div>
            <form className="Form" onSubmit={postHandler}>
                <h3>Add new post</h3>
                <input
                    className="Input"
                    type="text"
                    name="name"
                    placeholder="Your name"
                    value={post.title}
                    onChange={postDataChanged}
                />
                <textarea
                    className="Input"
                    type="text" name="discription"
                    placeholder="enter your text"
                    value={post.discription}
                    onChange={postDataChanged}
                />
                <button className="btn">Add post</button>
            </form>
        </div>
    );
};

export default Add;