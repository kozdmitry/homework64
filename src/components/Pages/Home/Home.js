import React, {useEffect, useState} from 'react';
import "./Home.css";
import axiosPosts from "../../../axiosPosts";
import moment from "moment";

const Home = () => {
    const [pageContent, setPageContent] = useState([]);

    useEffect(() => {
        axiosPosts.get('/posts/.json').then(result => {
            setPageContent(result.data)
        }, e => {
            console.log(e);
        });
    });

    return (
        <div className="Home">
            {Object.keys(pageContent).map(key => (
                <div className="post" key={key}>
                    <p>Create on: {moment(pageContent[key].date).format('dddd hh:mm')}</p>
                    <b>{pageContent[key].name}</b>
                    <p>Message: {pageContent[key].discription}</p>
                    <button className="btn">ReadMore</button>
                </div>
            ))}
        </div>
    );
};

export default Home;