import React from 'react';
import "./Contacts.css";

const Contacts = () => {
    return (
        <div className="Contacts">
            <div className="ContactBlock">
                <h2>Contact US</h2>
            </div>
            <div className="PhoneBlock">
                <p>Phone: +996 700 000 000</p>
                <p>Email: gggg@yandex.ru</p>
                <p>Str: B-Baatyra 176</p>
            </div>
        </div>
    );
};

export default Contacts;