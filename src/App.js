import React  from 'react';
import './App.css';
import MyPage from "./container/MyPage/MyPage";

const App = () => {
  return (
    <div className="App">
      <MyPage/>

    </div>
  );
}

export default App;
